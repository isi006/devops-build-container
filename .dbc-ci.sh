#!/bin/bash

# commands to run on 'dbc run-ci'

echo "exec .dbc-ci.sh, params: ${@}"

echo "Current dir: $(pwd)"

echo "Test env: ${SOME_VAR}"

echo "Terraform version: $(terraform --version)"

echo "Ansible version: $(ansible --version)"

echo "Helm version: $(helm version)"
