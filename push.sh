#!/usr/bin/env sh

. ./dbc_version.sh

docker push ${IMAGE_NAME}
docker push ${REGISTRY_IMAGE}:${MAJOR_VERSION}
docker push ${REGISTRY_IMAGE}:${MAJOR_VERSION}.${MINOR_VERSION}
docker push ${REGISTRY_IMAGE}:${BUILD_CONTAINER_VERSION}
