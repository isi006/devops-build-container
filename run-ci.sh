#!/bin/bash

if [[ -f /app/.dbc-ci.sh ]]; then
  script_present=1
  . /app/.dbc-ci.sh "${@}"
fi

if [[ -f ${HOME}/workspace/.dbc-ci.sh ]]; then
  script_present=1
  . ${HOME}/workspace/.dbc-ci.sh "${@}"
fi

if [[ -z ${script_present} ]]; then
  echo "Put a file '.dbc-ci.sh' in your project to run ci commands."
fi
