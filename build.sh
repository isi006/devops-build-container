#!/usr/bin/env sh

. ./dbc_version.sh

BUILD_CONTAINER_GIT_HASH="$(git rev-parse --short HEAD)"

envsubst '${BUILD_CONTAINER_VERSION}' < dbc_template > dbc
chmod +x dbc

TARGETARCH=$(uname -m | tr '[:upper:]' '[:lower:]')
if [ ${TARGETARCH} = "x86_64" ]; then
  TARGETARCH=amd64
elif [ ${TARGETARCH} = "aarch64" ]; then
  TARGETARCH=arm64
fi
TARGETOS=$(uname -s | tr '[:upper:]' '[:lower:]')

docker build . -t ${IMAGE_NAME} \
  -t ${REGISTRY_IMAGE}:${MAJOR_VERSION} \
  -t ${REGISTRY_IMAGE}:${MAJOR_VERSION}.${MINOR_VERSION} \
  -t ${REGISTRY_IMAGE}:${BUILD_CONTAINER_VERSION} \
  --build-arg TERRAFORM_VERSION=1.0.8 \
  --build-arg HELM_VERSION=3.7.0 \
  --build-arg KUBECTL_VERSION=1.21.0 \
  --build-arg SONOBUOY_VERSION=0.55.1 \
  --build-arg BUILD_CONTAINER_VERSION=${BUILD_CONTAINER_VERSION} \
  --build-arg BUILD_CONTAINER_GIT_HASH=${BUILD_CONTAINER_GIT_HASH} \
  --build-arg CONTAINER_IMAGE="${REGISTRY_IMAGE}" \
  --build-arg TARGETOS=${TARGETOS} \
  --build-arg TARGETARCH=${TARGETARCH} \
  "${@}"

