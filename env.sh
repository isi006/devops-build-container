if (ls ~/container-profile.d/*.sh > /dev/null 2>&1); then
  for file in ~/container-profile.d/*.sh; do
    source "${file}"
  done
fi

echo "devops build container (dbc): ${BUILD_CONTAINER_VERSION} (${BUILD_CONTAINER_GIT_HASH})"
echo

export SSH_AUTH_SOCK=/ssh-agent

if [[ -d ~/workspace ]]; then
  cd ~/workspace
fi
