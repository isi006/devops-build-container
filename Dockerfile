FROM ubuntu:20.04

ARG TARGETOS
ARG TARGETARCH

RUN apt-get update && DEBIAN_FRONTEND="noninteractive" TZ="Europe/Berlin" apt-get install -y \
    ca-certificates \
    software-properties-common \
    curl \
    wget \
    gnupg \
    python3 \
    python3-pip \
    unzip \
    iputils-ping \
    sudo \
    git \
    vim \
    jq \
    ssh \
    pkg-config \
    libcairo2-dev \
    libjpeg-dev \
    libgif-dev \
    dnsutils \
    iproute2 \
    rsync \
    s3cmd \
    pwgen \
    git-crypt \
    jq \
    gettext-base \
    bash-completion \
 && rm -rf /var/lib/apt/lists/*

# RUN mkdir -p ~/.docker/cli-plugins && \ 
#     wget -nv -O ~/.docker/cli-plugins/docker-buildx https://github.com/docker/buildx/releases/download/${BUILDX_VERSION}/buildx-${BUILDX_VERSION}.${BUILDX_ARCH} && \    
#     chmod a+x ~/.docker/cli-plugins/docker-buildx

COPY requirements.txt ansible-requirements.yml /tmp/

RUN set -e; \
  pip3 install --default-timeout=180 -r /tmp/requirements.txt; \
  rm /tmp/requirements.txt; \
  ansible-galaxy install -p /usr/share/ansible/collections -r /tmp/ansible-requirements.yml

ARG TERRAFORM_VERSION
RUN set -e; \
  cd /tmp; \
  curl -Ss -o terraform.zip https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_${TARGETOS}_${TARGETARCH}.zip; \
  unzip terraform.zip; \
  mv terraform /usr/local/bin/; \
  chmod +x /usr/local/bin/terraform; \
  rm terraform.zip

ARG HELM_VERSION
RUN set -e; \
  cd /tmp; \
  curl -Ss -o helm.tar.gz https://get.helm.sh/helm-v${HELM_VERSION}-${TARGETOS}-${TARGETARCH}.tar.gz; \
  tar xzf helm.tar.gz; \
  mv ${TARGETOS}-${TARGETARCH}/helm /usr/local/bin/; \
  chmod +x /usr/local/bin/helm; \
  rm -rf ${TARGETOS}-${TARGETARCH} helm.tar.gz

ARG KUBECTL_VERSION
RUN set -e; \
    cd /tmp; \
    curl -sLO "https://dl.k8s.io/release/v${KUBECTL_VERSION}/bin/${TARGETOS}/${TARGETARCH}/kubectl"; \
    mv kubectl /usr/local/bin/; \
    chmod +x /usr/local/bin/kubectl

ARG SONOBUOY_VERSION
RUN set -e; \
  cd /tmp; \
  curl -LSs -o sonobuoy.tar.gz https://github.com/vmware-tanzu/sonobuoy/releases/download/v${SONOBUOY_VERSION}/sonobuoy_${SONOBUOY_VERSION}_${TARGETOS}_${TARGETARCH}.tar.gz; \
  tar xzf sonobuoy.tar.gz; \
  mv sonobuoy /usr/local/bin/; \
  chmod +x /usr/local/bin/sonobuoy; \
  rm -rf sonobuoy.tar.gz

RUN set -e; \
  cd /tmp; \
  curl -LSs -o awscliv2.zip https://awscli.amazonaws.com/awscli-exe-linux-$(uname -m).zip; \
  unzip awscliv2.zip; \
  ./aws/install; \
  rm -rf ./aws awscliv2.zip;

RUN echo "%sudo ALL = (ALL) NOPASSWD: ALL" > /etc/sudoers.d/sudo_group

COPY env.sh /etc/profile.d/90-env.sh

ARG BUILD_CONTAINER_VERSION
ARG BUILD_CONTAINER_GIT_HASH
ARG CONTAINER_IMAGE
RUN set -e; \
  echo "export BUILD_CONTAINER_VERSION=${BUILD_CONTAINER_VERSION}" > /etc/profile.d/80-image-info.sh; \
  echo "export BUILD_CONTAINER_GIT_HASH=${BUILD_CONTAINER_GIT_HASH}" >> /etc/profile.d/80-image-info.sh; \
  echo "export CONTAINER_GIT_HASH=${BUILD_CONTAINER_GIT_HASH}" >> /etc/profile.d/80-image-info.sh; \
  echo "export CONTAINER_VERSION=${BUILD_CONTAINER_VERSION}" >> /etc/profile.d/80-image-info.sh; \
  echo "export CONTAINER_IMAGE=${CONTAINER_IMAGE}" >> /etc/profile.d/80-image-info.sh

COPY entrypoint.sh run-ci.sh dbc /

ENTRYPOINT [ "/entrypoint.sh" ]
