# devops-build-container

This container contains devops tools like Terraform, Ansible, kubectl or Helm.

## Development

One intent is to provide a container for developers. Just use a container to run and develop devops tasks without to take care about installing all the tools in the right versions.

### Requirements

* docker
* bash shell
* envsubst (https://command-not-found.com/envsubst)

### Usage

* install the [dbc](/dbc) script in your project directory:

#### latest version

```shell
id=$(docker create docker.io/isi006/devops-build-container:latest) && docker cp ${id}:/dbc . && docker rm ${id}
```

#### specific version

```shell
id=$(docker create docker.io/isi006/devops-build-container:2.3.0) && docker cp ${id}:/dbc . && docker rm ${id}
```

You can check this file into SCM but do not change it's content. It will be replaced with the next version.

* Start the container with `./dbc run`.
* Now you are in the container in the folder `~/workspace` where your project directory is mounted.
* The container is named after the current directory, check it with `docker ps` on your host.
* Another `./dbc run` in the same directory starts a new shell in same container.
* A `./dbc run` in a different directory start a new container.
* The container will be removed, if you are leave the container. Or use `./dbc stop` to destroy the container.
* Use `./dbc pull` to pull a fresh image from the registry.

### Special features

The container has special features to support the developer scenario:

* At container start the group und user is created on the fly, to match the current user of the host system. Having the same user in the container avoids file rights clashes in the mounted source code.
* Because 'su' is used to switch to the user, it is not possible to pass environment variables directly into the container (with docker run --env ...). To do this anyway you can add a directory called `container-profile.d` to your project. Every script in this directory will be sourced on startup of the container.
* If you are running a ssh-agent on the host, you can use it in container as well. The agent socket is mounted under `/ssh-agent` and the `SSH_AUTH_SOCK` is set to that path.
* The directories `~/.ssh`, `~/.azure` and `~/.gnupg` are also mounted in the container, to have these files in place.

### Configuration

Put a file `.dbc-conf` in your project directory to run a special image of the container:

```shell
DBC_TAG=latest
DBC_IMAGE_NAME=docker.io/isi006/devops-build-container
```

## Build

The second intention is to use the same container in build pipelines to eleminate incompatibilities between the developer setup and the build environment.

### Container builds

Docker is **not** installed in the container. Please use alternative technologies to build containers, like [Kaniko](https://github.com/GoogleContainerTools/kaniko). Developers should use the local docker installation.

### CI

Put a file `.dbc-ci.sh` with the commands you want to run in the container in your project directory and run `./dbc run-ci` in your pipeline.

## Build and extend the container

The container is Ubuntu 20.04 based and comes with the following tools:

A couple of Ubuntu packages, see the first RUN in the the [Dockerfile](Dockerfile).

Installed via pip, see the [requirements.txt](requirements.txt) file:

* Ansible core 2.11.5 plus a some additional python modules.
* Azure CLI 2.28.1

Direct downloads

* Terraform 1.0.8
* Helm 3.7.0
* kubectl 1.21.0
* sonobuoy 0.55.1
* aws cli (current version)

If you need other tool versions, fork this repo and change the versions in [build.sh](build.sh) and/or provide your own [requirements.txt](requirements.txt) to change the Ansible/Python setup.

Then run:

```shell
$ ./build.sh
```

You can extend the container if you need more tools with your own Dockerfile:

```docker
FROM isi006/devops-build-container:2

# ...install what you want

# copy your code to /app
COPY ansible /app/ansible

ARG CONTAINER_VERSION
ARG CONTAINER_GIT_HASH
ARG CONTAINER_IMAGE
RUN set -e; \
  echo "export CONTAINER_GIT_HASH=${CONTAINER_GIT_HASH}" >> /etc/profile.d/81-custom-image-info.sh; \
  echo "export CONTAINER_VERSION=${CONTAINER_VERSION}" >> /etc/profile.d/81-custom-image-info.sh; \
  echo "export CONTAINER_IMAGE=${CONTAINER_IMAGE}" >> /etc/profile.d/81-custom-image-info.sh

# copy your commands to execute on 'dbc run-ci'
COPY .dbc-ci.sh /app/.dbc-ci.sh
```

The `app` directory is special, because the entrypoint will adapt this directory für your current user (chown -R ... /app).

A custom image can come with special commands to execute in `dbc run-ci`. Just put a `.dbc-ci.sh` in the `/app` directory.

Providing the CONTAINER_VERSION, CONTAINER_GIT_HASH, CONTAINER_IMAGE build parameters is necessary to enable the version check on container start.

## Constribute

Yes, I know, there are so many missing tools...

Please feel free to send me PRs for useful extensions of the container.
