#!/usr/bin/env bash

set -e

USER_ID=${1:-1000}
shift
USER_NAME=${1:-run}
shift
GROUP_ID=${1:-1000}
shift
GROUP_NAME=${1:-run}
shift
EXEC_CI=${1:-1}
shift
DBC_SCRIPT_VERSION=${1:-1}
shift
PARAMS="${@}"

for f in /etc/profile.d/*image-info.sh; do
  . ${f}
done

if [[ ! -z "${DBC_SCRIPT_VERSION}" ]]; then
  if [[ "${DBC_SCRIPT_VERSION}" != "${BUILD_CONTAINER_VERSION}" ]]; then
    echo "###### WARNING: Version missmatch ######"
    echo "The dbc script version is ${DBC_SCRIPT_VERSION} and the container is based on version ${BUILD_CONTAINER_VERSION}"
    echo "  Please leave the container and update your dbc script with this command:"
    echo "  id=\$(docker create ${CONTAINER_IMAGE}:${CONTAINER_VERSION}) && docker cp \${id}:/dbc . && docker rm \${id}"
    echo "########################################"
  fi
fi

HOME_DIR="/home/$(echo -n ${USER_NAME} | sed 's/\\/\//g')"

mkdir -p ${HOME_DIR}

if ! (getent group ${GROUP_NAME}); then
  groupadd -g ${GROUP_ID} ${GROUP_NAME}
fi
if ! (getent passwd ${USER_NAME}); then
  useradd -s /bin/bash ${USER_NAME} -u ${USER_ID} -g ${GROUP_ID} -G sudo -d ${HOME_DIR}
fi
cp -a /etc/skel/. ${HOME_DIR}/

chown ${USER_ID}:${GROUP_ID} ${HOME_DIR}/.*

if [[ -d /app ]]; then
  chown -R ${USER_ID}:${GROUP_ID} /app
fi

if [[ ${EXEC_CI} == "0" ]]; then
  su - ${USER_NAME}
else
  su - ${USER_NAME} -c "/run-ci.sh ${PARAMS}"
fi
